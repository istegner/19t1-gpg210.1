﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MillipedeController : MonoBehaviour
{
    public int health;
    public float speed;
    public float poisonedSpeed;
    public GameObject mushroomPrefab;
    public AudioClip clip; //Death SFX

    Vector3 direction;
    float upDown; //Are we going up or down?
    AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        direction = Vector3.right;
        upDown = -1; //Going down
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    void Move()
    {
        transform.Translate((direction * speed) * Time.deltaTime); //Lets move
    }

    /// <summary>
    /// When we collide with a mushroom, move up or down
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Mushroom"))
        {
            MoveUpDown();
        }
    }

    /// <summary>
    /// When we collide with a wall, go up or down
    /// If we collide with a top or bottom wall, change if we are going up or down
    /// When we collide with a bullet, add score and destroy itself and play death sfx
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Wall"))
        {
            MoveUpDown();
        }

        if (collision.gameObject.CompareTag("WallBottom"))
        {
            upDown = 1;
        }
        else if (collision.gameObject.CompareTag("WallTop") || collision.gameObject.CompareTag("MiddleWall"))
        {
            upDown = -1;
        }

        if (collision.gameObject.CompareTag("Bullet"))
        {
            Destroy(collision.gameObject);
            audioSource.PlayOneShot(clip);
            this.GetComponent<MeshRenderer>().enabled = false;
            GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().score += 100;
            Instantiate(mushroomPrefab, new Vector3(Mathf.RoundToInt(transform.position.x),Mathf.RoundToInt(transform.position.y),0), transform.rotation);
            this.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Change directions and move up or down 1 block
    /// </summary>
    void MoveUpDown()
    {
        direction = -direction;
        var offset = Mathf.RoundToInt(transform.position.y) - transform.position.y;
        transform.position = new Vector3(Mathf.RoundToInt(transform.transform.position.x), Mathf.RoundToInt(transform.position.y), Mathf.RoundToInt(transform.position.z));
        transform.position = new Vector3(transform.position.x + offset, transform.position.y + upDown, transform.position.z);
    }
}
