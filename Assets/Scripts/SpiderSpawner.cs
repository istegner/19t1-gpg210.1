﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderSpawner : MonoBehaviour
{
    public Transform spawnPoint;
    public GameObject spiderPrefab;
    [SerializeField]int spawnDelay;
    public Vector3 offset = new Vector3(0,0,0.75f); //The spider's GO is offset by 0.75 on the z-axis

    private void Start()
    {
        spawnDelay = Random.Range(10, 30);
        StartCoroutine(TakeOne());
    }

    // Update is called once per frame
    void Update()
    {
        if(spawnDelay <= 0)
        {
            Spawn();
        }
    }

    void Spawn()
    {
        spawnDelay = Random.Range(10, 30); //Pick a random time to spawn a new spider
        Instantiate(spiderPrefab, spawnPoint.transform.position + offset, spawnPoint.transform.rotation);
    }

    /// <summary>
    /// Take 1 from the time to spawn a spider after 1 second
    /// </summary>
    /// <returns></returns>
    IEnumerator TakeOne()
    {
        yield return new WaitForSeconds(1);
        spawnDelay--;
        StartCoroutine(TakeOne());
    }
}
