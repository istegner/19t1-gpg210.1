﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Spawn a new millipede when the current one dies
/// </summary>
public class MillipedeManager : MonoBehaviour
{
    public GameObject millipedePrefab;

    // Update is called once per frame
    void Update()
    {
        if (CheckMillipede())
        {
            Respawn();
        }
    }

    void Respawn()
    {
        Instantiate(millipedePrefab);
    }

    bool CheckMillipede()
    {
        var check = GameObject.FindGameObjectsWithTag("Millipede");
        if(check.Length == 0)
        {
            return true;
        }
        return false;
    }
}
