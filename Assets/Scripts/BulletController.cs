﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public int damage;
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        Destroy(this.gameObject, 15); //Destroy ourself after 15 seconds just in case we excape the map
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    void Move()
    {
        transform.Translate((Vector3.up * speed) * Time.deltaTime);
    }

    /// <summary>
    /// Destroy itself if we collide with a wall
    /// We don't want to destroy if we hit the middle wall
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Contains("Wall") && !collision.gameObject.CompareTag("MiddleWall"))
        {
            Destroy(this.gameObject);
        }
    }
}
