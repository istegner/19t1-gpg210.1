﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MushroomController : MonoBehaviour
{
    public int health;
    public bool isInPlayArea;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Play Area"))
            isInPlayArea = true;
        else
            isInPlayArea = false;

        //If a bullet hit the mushroom and no more health, add some score and destroy itself
        if (other.gameObject.CompareTag("Bullet"))
        {
            Destroy(other.gameObject);
            health--;
            if (health <= 0)
            {
                GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().score += 1;
                Destroy(gameObject);
            }
        }
    }
}
