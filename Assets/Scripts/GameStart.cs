﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStart : MonoBehaviour
{
    public GameObject mushroomPrefab; //The mushroom prefab
    public Transform leftWall; //The left wall
    public Transform rightWall; //The right wall
    public Transform topWall; //The top wall
    public Transform bottomWall; //The bottom wall
    public int initialMushroomsToSpawn; //How many mushrooms to spawn at the start

    //A list of all possible spawn points
    [SerializeField] List<Vector3> spawnList = new List<Vector3>();

    // Start is called before the first frame update
    void Start()
    {
        //Populate the list of spawn points
        //+- on the positions so we don't spawn on the edges (fixes some bugs with millipede escaping map)
        for (int x = (int)leftWall.position.x + 2; x < rightWall.position.x - 1; x++)
        {
            for (int y = (int)bottomWall.position.y + 2; y < topWall.position.y - 1; y++)
            {
                spawnList.Add(new Vector3(x, y, 0));
            }
        }

        //Shuffle the list
        for(int i = 0; i< spawnList.Count; i++)
        {
            var rand = Random.Range(0, spawnList.Count);
            var temp = spawnList[rand];
            spawnList[rand] = spawnList[i];
            spawnList[i] = temp;
        }

        for (int i = 0; i < initialMushroomsToSpawn; i++)
        {
            Spawn(spawnList[i]);
        }
    }

    /// <summary>
    /// Spawn some mushrooms within the walls
    /// </summary>
    void Spawn(Vector3 spawnPoint)
    {
        // Instantiate the Mushroom
        Instantiate(mushroomPrefab, spawnPoint, Quaternion.identity);
    }
}
