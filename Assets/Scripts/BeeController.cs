﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Broken
/// </summary>
public class BeeController : MonoBehaviour
{
    public float speed;
    public int health;
    public int numberOfMushroomsToPlace;
    public GameObject mushroomPrefab;

    GameObject gameManager;

    int height;
    int prevHeight;
    int nextHeight;

    // Start is called before the first frame update
    void Start()
    {
        numberOfMushroomsToPlace = Random.Range(10, 15);
        gameManager = GameObject.FindGameObjectWithTag("GameManager");
        prevHeight = Mathf.RoundToInt(transform.position.y);
    }

    // Update is called once per frame
    void Update()
    {
        Move();

        height = Mathf.RoundToInt(transform.position.y);
        nextHeight = height++;
        if (height + 8 >= numberOfMushroomsToPlace)
        {
            if (height == nextHeight)
            {
                Instantiate(mushroomPrefab, new Vector3(transform.position.x, height, 0), transform.rotation);
                numberOfMushroomsToPlace--;
                prevHeight++;
            }
        }
    }

    void Move()
    {
        transform.Translate((Vector3.down * speed) * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("WallBottom"))
        {
            Destroy(gameObject);
        }

        if (collision.gameObject.CompareTag("Bullet"))
        {
            Destroy(collision.gameObject);
            health--;
            if (health <= 0)
            {
                gameManager.GetComponent<GameManager>().score += 200;
                Destroy(gameObject);
            }
        }
    }
}
