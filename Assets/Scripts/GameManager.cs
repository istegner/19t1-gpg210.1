﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{
    public List<Transform> field = new List<Transform>();
    public int level;
    public int score;

    public TMP_Text scoreText;
    public GameObject pauseMenu;

    int mushroomsInPlayArea;
    bool paused = false;

    // Start is called before the first frame update
    void Start()
    {
        //Cursor.lockState = CursorLockMode.Locked;
        StartCoroutine(GetFIeld());
        //StartCoroutine(SpawnBees());
        score = 0;
    }

    // Update is called once per frame
    void Update()
    {
        Physics.Simulate(Time.deltaTime); //Force simulate the physics every frame

        //Pause/resume the game when esc key is pressed
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (paused)
                Resume();
            else
                Pause();
        }
        //Restart the game when F1 key is pressed
        if (Input.GetKeyDown(KeyCode.F1))
        {
            SceneManager.LoadScene(0);
        }
        scoreText.text = "Score: " + score.ToString();

        if (Input.GetKeyDown(KeyCode.F12))
        {
            this.GetComponent<ShowFPS>().enabled = !this.GetComponent<ShowFPS>().enabled;
        }
    }

    /// <summary>
    /// Get the number of mushrooms and ddt's in the field
    /// </summary>
    /// <returns></returns>
    IEnumerator GetFIeld()
    {
        yield return new WaitForSeconds(1);
        field.Clear();
        GameObject[] mushrooms = GameObject.FindGameObjectsWithTag("Mushroom");
        GameObject[] ddts = GameObject.FindGameObjectsWithTag("DDT");
        if (mushrooms.Length > 0)
        {
            for (int i = 0; i < mushrooms.Length; i++)
            {
                field.Add(mushrooms[i].transform);
            }
        }
        if (ddts.Length > 0)
        {
            for (int i = 0; i < ddts.Length; i++)
            {
                field.Add(ddts[i].transform);
            }
        }
        StartCoroutine(GetFIeld());
    }

    //Get the number of mushrooms in the play area
    void MushroomsInPlayArea()
    {
        mushroomsInPlayArea = 0;
        for (int i = 0; i < field.Count; i++)
        {
            if (field[i].GetComponent<MushroomController>().isInPlayArea)
            {
                mushroomsInPlayArea++;
            }
        }
    }

    /// <summary>
    /// Resume the game
    /// </summary>
    public void Resume()
    {
        paused = !paused;
        Time.timeScale = 1;
        pauseMenu.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
    }

    /// <summary>
    /// Pause the game
    /// </summary>
    void Pause()
    {
        paused = !paused;
        Time.timeScale = 0;
        pauseMenu.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
    }

    /// <summary>
    /// What to do when the exit button is pressed
    /// </summary>
    public void ExitBtn()
    {
        SceneManager.LoadScene(0);
    }
}
