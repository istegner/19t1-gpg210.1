﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Manage the audio, shooting and movement of the player
/// </summary>
public class PlayerController : MonoBehaviour
{
    public GameObject bulletPrefab; //What bullet to spawn
    public Transform bulletSpawn; //Where to spawn the bullets
    public AudioClip clip; //The shoot sfx

    public int health;
    public float lives;
    public float moveSpeed;
    AudioSource audioSource;

    Rigidbody rb;

    GameObject bulletRef; //The bullet that the player has fired

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        rb = this.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();

        if (Input.GetKey(KeyCode.Space) && CheckShoot())
        {
            Shoot();
        }

        if (health <= 0)
        {
            Die();
        }
    }

    /// <summary>
    /// Get the direction and velocity to move at
    /// </summary>
    void Move()
    {
        var movHori = Input.GetAxis("Horizontal");
        var movVert = Input.GetAxis("Vertical");
        var dir = new Vector3(movHori, movVert, 0).normalized * moveSpeed;
        rb.velocity = dir;
        if (dir == Vector3.zero)
        {
            rb.velocity = Vector3.zero;
        }
    }

    void Shoot()
    {
        audioSource.PlayOneShot(clip);
        bulletRef = Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);
    }

    bool CheckShoot()
    {
        if (bulletRef == null)
        {
            return true;
        }
        return false;
    }

    void Die()
    {
        lives--;
        if (lives <= 0)
        {
            Debug.Log("Game Over");
            return;
        }
        health++;
        Debug.Log("You died. Live remaining: " + lives);
    }

    /// <summary>
    /// Restart the game when collided with an enemy
    /// TODO: Use a life based system
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy") || collision.gameObject.CompareTag("Millipede"))
        {
            SceneManager.LoadScene(0);
        }
    }
}
