﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

/// <summary>
/// Handle the buttons and text elements for the main menu
/// </summary>
public class MainMenuController : MonoBehaviour
{
    public TMP_Text title;
    public TMP_Text versionNumber;
    public TMP_Text companyName;

    // Start is called before the first frame update
    void Start()
    {
        title.text = Application.productName;
        versionNumber.text = "v" + Application.version;
        companyName.text = Application.companyName;
    }

    /// <summary>
    /// What to do when the play button is pressed
    /// </summary>
    public void PlayBtn()
    {
        SceneManager.LoadScene(1);
    }

    /// <summary>
    /// What to do when the exit button is pressed
    /// </summary>
    public void ExitBtn()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
}
