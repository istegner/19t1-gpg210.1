﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderController : MonoBehaviour
{
    public float moveWait;
    public int health;
    public Vector3[] directions;

    Vector3 direction;
    bool canMove;

    public AudioClip clip;
    AudioSource source;

    // Start is called before the first frame update
    void Start()
    {
        canMove = true;
        source = this.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    void Move()
    {
        //If can move, move in a random direction unless as high as play area, then go down
        if (canMove)
        {
            canMove = false;
            var height = transform.position.y;
            if(height >= -5)
            {
                direction = directions[2];
            }
            else
            {
                direction = directions[Random.Range(0, directions.Length)];
            }

            source.PlayOneShot(clip); //Play a sound effect when moved
            transform.position = transform.position + direction;
            StartCoroutine(ResetMove());
        }

        //If goes out of the map, die
        if(transform.position.x <= -11)
        {
            Destroy(this.gameObject);
        }
    }

    /// <summary>
    /// Allow the spider to move again
    /// </summary>
    /// <returns></returns>
    IEnumerator ResetMove()
    {
        yield return new WaitForSeconds(moveWait);
        canMove = true;
    }

    /// <summary>
    /// Eat a mushroom when collided with
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Mushroom"))
        {
            Destroy(other.gameObject);
        }
    }

    /// <summary>
    /// When shot by a bullet, destroy itself and add some score
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            Destroy(collision.gameObject);
            GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().score += 300;
            Destroy(this.gameObject);
        }
    }
}
